import os
import json
from jinja2 import Environment, FileSystemLoader


JSON_DIR = 'json'
TEMPLATE_DIR = 'j2'

with open(file=os.path.join(JSON_DIR,'experiences.json'), mode="r",encoding='utf-8' ) as experiences_file:
    experiences = json.load(experiences_file)

with open(file=os.path.join(JSON_DIR,'miscs.json'), mode="r" ,encoding='utf-8' ) as miscs_file:
    miscs = json.load(miscs_file)

with open(file=os.path.join(JSON_DIR,'projects.json'), mode="r" ,encoding='utf-8') as projects_file:
    projects = json.load(projects_file)


file_loader = FileSystemLoader(TEMPLATE_DIR)
env = Environment(loader=file_loader)


template = env.get_template('cv.j2')


output = template.render(experiences=experiences,miscs=miscs,projects=projects)

with open(file='CV_Emerik_NICOLE.html', mode='w', encoding='utf-8') as f:
    f.write(output)
